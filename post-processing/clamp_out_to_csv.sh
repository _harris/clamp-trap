#!/bin/bash
# Example code for processing CLAMP output files into a single CSV file.
#
#	Note default delimiter is tab character (\t).
#
#	Usage: ./clamp_to_csv.sh ./output_directory > example.csv
#
dir=$1
cd $dir
gawk '{split(FILENAME, b, "."); print b[1]"\t"$0}' *.txt | grep -v "CUI"
