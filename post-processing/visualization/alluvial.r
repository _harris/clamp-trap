# This script uses the networkD3 library to generate an alluvial chart.
#
#	Input: JSON file with nodes with links
#	Output: a preview of the links and nodes for QA and the plot
#
library(networkD3)

Smoking <- jsonlite::fromJSON("smoking_span_most_charted_example.json")


# 2 data frames: 
# one with links and a weight (from, to, value)
head( Smoking$links )
# and one with node names
head( Smoking$nodes )

# Plot as sankey with parameters for smoking
p <- sankeyNetwork(Links = Smoking$links, Nodes = Smoking$nodes, Source = "source",
                   Target = "target", Value = "value", NodeID = "name",
                   units = "Patients", fontSize = 12, nodeWidth = 30)
p

