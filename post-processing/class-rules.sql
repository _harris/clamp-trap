-- This script takes entries in the majority table and does two things:
-- 	(1) exclude things based on simple patterns
--	(2) flip classes based on simple patterns
--
--	class_final contains the final judgement; it will be blank if discarded
--
create view tobacco_majority_vw as
with x as (
	select m.*,
		-- exposure cases
		case when upper(problem_text) like '%EXPOS%'
			or upper(problem_text) like '%EXPSR%'
		-- second-hand or passive smoking 
			or upper(problem_text) like '%2ND%' 
			or upper(problem_text) like '%SECOND%HAND%' 
			or upper(problem_text) like '%COHAB%' 
			or upper(problem_text) like '%PARENTS%'
		-- smoke (as in a fire)
			or upper(problem_text) like '%CONFLAGRATION%' 
			or upper(problem_text) like '%FIRE%' 
			or upper(problem_text) like '%FLAMES%' 
			or upper(problem_text) like '%FUMES%' 
			or upper(problem_text) like '%INHALATION%' 
			or upper(problem_text) like '%GRILL%FOOD%'
		-- family smoking status
			or upper(problem_text) like '%FAMILY%H%'	or upper(problem_text) like '%FAMILY%MEM%'
			or upper(problem_text) like '%FHX%'	or upper(problem_text) like '%FH%'
			or upper(problem_text) like '%HISTORY UNKNOWN%'
		-- maternal exposure
			or upper(problem_text) like '%MATERNAL%'
			or upper(problem_text) like '%PLACENTA%'	or upper(problem_text) like '%PRENATAL%'
		-- not tobacco
			or upper(problem_text) like '%MARIJUANA%'
			or upper(problem_text) like '%DRUGS%'
		-- environmental
			or upper(problem_text) like '%DETECTOR%'
			or upper(problem_text) like '%GUIDANCE%'	or upper(problem_text) like '%ASSESS%'
			or upper(problem_text) like '%NOT DONE%'	or upper(problem_text) like '%VITAL%'
			or upper(problem_text) like '%TOXIC EFFECT OF SMOKE%'
			or upper(problem_text) like '%POISONING BY SMOKE%'
			or (upper(problem_text) like '%CAREGIVER%' and upper(problem_text) not like '%ENABLING%')
		-- unknown status is not helpful
			or (upper(problem_text) like '%UNKNOWN%' and upper(problem_text) not like '%UNKNOWN AMOUNT%')
			then 1 else 0 end as exclude_flag
	from clamp_tobacco__majority m
), rules as (
	select x.*,
		case
			-- If it's not tobacco w/ uncertainty around quitting or definitive smoking clues
			when x.exclude_flag = 0 and x.clamp_class <> 'Tobacco'
				and (	 x.problem_text like '%NEVER TRIED TO QUIT%' 
						or x.problem_text like '%HAS REDUCED%'
						or x.problem_text like '%HAS SMOKED%'
						or x.problem_text like '%UNSUCCESSFULLY%'
						or x.problem_text like '%NOT READY%'
						or x.problem_text like '%READY TO QUIT%'
						or x.problem_text like '%LIKE TO QUIT%'
						or x.problem_text like '%NOT CONSIDERING%'
						or x.problem_text like '%DECLINE%CESSATION%'
						or x.problem_text like '%MAINTENANCE THERAPY%'
						or x.problem_text like '%PATIENT SMOKING%'
					)
				then 'Tobacco' 
			-- Cessation/remission/history only makes sense in the context of prior use
			when x.exclude_flag = 0 and x.clamp_class <> 'PastTobacco'
				and (	 x.problem_text like '%EX-%' 
						or x.problem_text like '%CESSATION OF%'
						or x.problem_text like '%FORMER%'
						or x.problem_text like '%REMISSION%'
						or x.problem_text like '%DISCONTINUED%'
						or x.problem_text like '%HISTORY OF%'
						or x.problem_text like '%HX%'
						or x.problem_text like '%PAST USE%'
						or (x.problem_text like '%WITHDRAWAL%' and x.problem_text not like '%WITHDRAWAL STATE%')
						or x.problem_text like '%RECENTLY QUIT%'
						or x.problem_text like '%RECENTLY STOP%'
						or x.problem_text like '%QUIT USING%'
					)
				then 'PastTobacco' 
			-- non-status and abstinence indicates NonTabacco
			when x.exclude_flag = 0 and x.clamp_class <> 'NonTobacco'
				and (
						x.problem_text like '%NON-USER%'
						or x.problem_text like '%NON-TOBACCO%'
						or x.problem_text like '%ABSTINENCE%'
				) then 'NonTobacco'
		end as rule_judge
	from x
)
select *, 
	-- basic judgement if the rule produced something the majority agreed upon
	case when rule_judge = class_judge then 1 else 0 end as rule_correct,
	-- class_final is either the correct answer from CLAMP or the judgees answer post-processing
	case 	when correct = 1 then clamp_class 
			when rule_correct = 1 then rule_judge end as class_final
from rules r
