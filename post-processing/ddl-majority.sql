-- This table can hold the majority vote for reviewers analyzing CLAMP.
--
-- Each row represents a problem and the assigned class determined by the majority.

create table clamp_tobacco_majority (
	id integer not null,			-- unique ID for this problem
	clamp_class varchar(11) not null,	-- CLAMP's class
	class_judge varchar(11),		-- Judge assigned class via majority rules
	correct integer,			-- 1 if clamp_class = class_judge
	smoking integer,			-- 1 if this problem is about smoking in particular
	context_sensitive integer,		-- 1 if this problem is context sensitive (see paper for discussion)
	problem_text varchar(255)		-- text associated with this problem
);

