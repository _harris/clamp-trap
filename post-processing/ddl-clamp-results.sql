-- DDL for storing CLAMP results in a relational database
--
create table clamp_tobacco_status (
	id		int,
	start_pos	int,
	end_pos		int,
	class		varchar(12),
	cui		varchar(12),
	assertion	varchar(20),
	entity		varchar(50)
);

