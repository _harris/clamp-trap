# README #

This repository contains code for Tobacco-Related Analyses of Problem-Lists (TRAPs), a companion to CLAMP that focuses on pre-processing and post-processing of NLP results.

### Contents ###

* Pre-proccessing input for CLAMP
* Post-processing results
* Rules for exclusion and adjusting class assignments
* Example of time span creation for tobacco statuses
* Alluvial diagram of status transitions
