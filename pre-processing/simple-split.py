# simple-split.py:   
# 	Given a CSV file, split them into separate files for processing.
#
#	The below code is an example of splitting a simple CSV with two columns: ID and TEXT.
#	
#	Input file is smoking-problems.csv and output is a directory of files
#		Each file will be called ID.txt and will contain the TEXT as contents.
#	
import csv

fileout = ''

with open("smoking-problems.csv") as csvfile:
	reader = csv.DictReader(csvfile, delimiter=',')
	for row in reader:
		fileout = open("./smoking-problems/" + str(row['ID']) + '.txt', 'w')
		fileout.write(row['TEXT'])
		fileout.close()
csvfile.close()
